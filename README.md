# spring-boot-elasticsearch-crud
Elasticsearch is a search engine based on the Lucene library. It provides a distributed, multitenant-capable full-text search engine with an HTTP web interface and schema-free JSON documents. Elasticsearch is developed in Java and is the most popular enterprise search engine. Elasticsearch provides near real-time search and analytics for all types of data. Whether you have structured or unstructured text, numerical data, or geospatial data, Elasticsearch can efficiently store and index it in a way that supports fast searches.


## Version compatibility chart

| Elasticsearch | Springboot | Java       |
| :-----------: | :--------: | :--------: |
| 6.2.2         | 2.1.x      |   8 - 12   |
| 6.8.12        | 2.2.x      |   8 - 15   |
| 7.6.2         | 2.3.x      |   8 - 15   |
| 7.9.3         | 2.4.x      |   8 - 16   |
| 7.12.0        | 2.5.x      |   8 - 18   |
| 7.15.2        | 2.6.x      |	 8 - 19   |
| 7.17.3        | 2.7.x      |   8 - 21   |
| 8.5.3         | 3.0.x      |  17 - 21   |
| 8.7.1         | 3.1.x      |	17 - 21   |
| 8.11.1        | 3.2.x      |	17 - 21   |

## Versions Used
1. Spring boot Version: 2.4.0-SNAPSHOT
2. Spring Data Elasticsearch: 4.1.1
3. Elasticsearch Client: 7.9.3
4. Elasticsearch Database Cluster: 7.9.3

> It is important to match the Elasticsearch Cluster and Elasticsearch Client versions, otherwise the code does not work   

## Commands to run Elasticsearch docker container

```
# Get the Supported Elasticsearch version to match the Elasticsearch client 
docker pull elasticsearch:7.9.3

# Run the cluster in single node
docker run -d -p 9200:9200 -p 9300:9300 --name elasticsearch-7.9.3 -e "discovery.type=single-node" elasticsearch:7.9.3
```

## Build the Project skipping Tests

```
mvn clean install -DskipTests
```

## Run the Project

```
# using maven
mvn spring-boot:run

# using java 
java -jar spring-boot-elasticsearch-crud-0.0.1-SNAPSHOT.jar
```

## Rest APIs
GET request to http://localhost:8080/rest/movies to get all the movies

POST request to http://localhost:8080/rest/movie to add a new movie

```
{
	"id": null,
	"name": "Avengers"
}
```

PUT request to http://localhost:8080/rest/movie/<id> to update movie name

```
{
	"id": "id",
	"name": "Thor"
}
```
