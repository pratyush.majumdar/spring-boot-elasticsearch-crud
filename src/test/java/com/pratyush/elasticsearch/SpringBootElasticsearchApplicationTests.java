package com.pratyush.elasticsearch;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pratyush.elasticsearch.entity.Movie;

@SpringBootTest
@AutoConfigureMockMvc
class SpringBootElasticsearchApplicationTests {
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void testGetMovies() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/rest/movies"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
			.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void testAddMovie() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/rest/movie")
				.content(asJsonString(new Movie(null, "My Movie")))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
			.andExpect(MockMvcResultMatchers.status().isCreated())
			.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void testGetMovieById() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/rest/movie/{id}", "rtZMgYwBsUjCBkNZblZGc"))
			.andExpect(MockMvcResultMatchers.status().isOk())
			.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON))
			.andDo(MockMvcResultHandlers.print());
	}
	
	@Test
	public void testUpdateMovie() throws Exception {
		mockMvc.perform( MockMvcRequestBuilders.put("/rest/movie/{id}", "rtZMgYwBsUjCBkNZblZGc")
				.content(asJsonString(new Movie("rtZMgYwBsUjCBkNZblZGc", "My New Movie")))
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andDo(MockMvcResultHandlers.print());
	}
	
	private String asJsonString(final Object obj) {
	    try {
	        return new ObjectMapper().writeValueAsString(obj);
	    } catch (Exception e) {
	        throw new RuntimeException(e);
	    }
	}
}
