package com.pratyush.elasticsearch.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Document(indexName = "movies1")
@Getter @Setter @AllArgsConstructor
public class Movie {
	@Id
	private String id;
	
	private String name;
}
