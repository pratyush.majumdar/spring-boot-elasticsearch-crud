package com.pratyush.elasticsearch.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pratyush.elasticsearch.entity.Movie;
import com.pratyush.elasticsearch.repository.MovieRepository;

@Service
public class MovieService {
	@Autowired
	private MovieRepository movieRepository;
	
	public Optional<Movie> getMovieById(String id) {
		return movieRepository.findById(id);
	}
	
	public List<Movie> getAllMovies() {
		List<Movie> movies = new ArrayList<>();
		Iterator<Movie> iterator = movieRepository.findAll().iterator();
		while(iterator.hasNext()) {
			movies.add(iterator.next());
		}
		return movies;
	}
	
	public void addMovie(Movie movie) {
		movieRepository.save(movie);
	}
	
	public void updateMovie(String name, Movie movie) {		
		movieRepository.save(movie);
	}
}
