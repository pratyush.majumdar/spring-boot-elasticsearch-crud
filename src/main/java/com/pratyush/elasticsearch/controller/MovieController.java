package com.pratyush.elasticsearch.controller;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.pratyush.elasticsearch.entity.Movie;
import com.pratyush.elasticsearch.service.MovieService;

@RestController
public class MovieController {
	@Autowired
	private MovieService movieService;
	
	@GetMapping(value="/rest/movies")
	public List<Movie> getAllMovies() {
		return movieService.getAllMovies();
	}
	
	@GetMapping(value="/rest/movie/{id}")
	public Optional<Movie> getMovie(@PathVariable String id) {
		return movieService.getMovieById(id);
	}
	
	@PostMapping(value="/rest/movie")
	public ResponseEntity<Movie> addMovie(@RequestBody Movie movie) {
		try {
			movieService.addMovie(movie);
			return new ResponseEntity<Movie>(movie, HttpStatus.CREATED);
		}catch(Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(value="/rest/movie/{id}")
	public ResponseEntity<Movie> updateMovie(@RequestBody Movie movie, @PathVariable String id) {
		try {
			movieService.updateMovie(id, movie);
			return new ResponseEntity<Movie>(movie, HttpStatus.OK);
		}catch (NoSuchElementException e) {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}
}
