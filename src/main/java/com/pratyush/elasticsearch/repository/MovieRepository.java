package com.pratyush.elasticsearch.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

import com.pratyush.elasticsearch.entity.Movie;

@Repository
public interface MovieRepository extends ElasticsearchRepository<Movie, String> {

}
